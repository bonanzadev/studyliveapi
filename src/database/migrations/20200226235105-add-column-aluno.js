module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('aluno', 'plano_id', {
      type: Sequelize.INTEGER,
      references: { model: 'plano', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true,
      after: 'id'
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('aluno', 'plano_id');
  }
};
