module.exports = {
  up: queryInterface => {
    return queryInterface.removeColumn('plano', 'excluido');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('plano', 'excluido', {
      type: Sequelize.STRING
    });
  }
};
