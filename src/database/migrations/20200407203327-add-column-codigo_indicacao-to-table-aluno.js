module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('aluno', 'codigo_indicacao', {
      type: Sequelize.STRING(20)
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('aluno', 'codigo_indicacao');
  }
};
