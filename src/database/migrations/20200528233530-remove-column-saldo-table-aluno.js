module.exports = {
  up: queryInterface => {
    return queryInterface.removeColumn('aluno', 'saldo');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('aluno', 'saldo', {
      type: Sequelize.STRING
    });
  }
};
