module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('aluno', 'avatar_id', {
      type: Sequelize.INTEGER,
      references: { model: 'file', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('aluno', 'avatar_id');
  }
};
