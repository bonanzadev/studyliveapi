module.exports = {
  up: queryInterface => {
    return queryInterface.removeColumn('professor', 'avatar');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('professor', 'avatar', {
      type: Sequelize.STRING
    });
  }
};
