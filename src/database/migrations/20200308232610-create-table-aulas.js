module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('aula', {
      id: {
        type: Sequelize.INTEGER, // tipo inteiro
        allowNull: false, // nao pode ser null
        autoIncrement: true, // auto increment
        primaryKey: true // chave primaria da tabela
      },
      aluno_id: {
        type: Sequelize.INTEGER,
        references: { model: 'aluno', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true
      },
      professor_id: {
        type: Sequelize.INTEGER,
        references: { model: 'professor', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'SET NULL',
        allowNull: true
      },
      data_inicio: {
        type: Sequelize.DATE,
        allowNull: false
      },
      data_fim: {
        type: Sequelize.DATE,
        allowNull: true
      },
      agendada: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: true
      },
      aconteceu: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: false
      },
      channel: {
        type: Sequelize.STRING,
        allowNull: true
      },
      avaliacao_professor: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      avaliacao_professor_mensagem: {
        type: Sequelize.STRING,
        allowNull: true
      },
      avaliacao_aluno: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      avaliacao_aluno_mensagem: {
        type: Sequelize.STRING,
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('aula');
  }
};
