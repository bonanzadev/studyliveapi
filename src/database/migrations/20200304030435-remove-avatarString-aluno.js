module.exports = {
  up: queryInterface => {
    return queryInterface.removeColumn('aluno', 'avatar');
  },

  down: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('aluno', 'avatar', {
      type: Sequelize.STRING
    });
  }
};
