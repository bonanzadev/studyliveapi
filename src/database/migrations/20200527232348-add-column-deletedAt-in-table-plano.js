module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('plano', 'deleted_at', {
        type: Sequelize.DATE
      }),
      queryInterface.addColumn('plano', 'experimental', {
        type: Sequelize.BOOLEAN
      })
    ]);
  },

  down: queryInterface => {
    return Promise.all([
      queryInterface.removeColumn('plano', 'deleted_at'),
      queryInterface.removeColumn('plano', 'experimental')
    ]);
  }
};
