module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('aluno', 'alunoindicou_id', {
      type: Sequelize.INTEGER,
      references: { model: 'aluno', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('aluno', 'alunoindicou_id');
  }
};
