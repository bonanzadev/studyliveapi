module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('professor', {
      id: {
        type: Sequelize.INTEGER, // tipo inteiro
        allowNull: false, // nao pode ser null
        autoIncrement: true, // auto increment
        primaryKey: true // chave primaria da tabela
      },
      nome: {
        type: Sequelize.STRING,
        allowNull: false
      },
      apelido: {
        type: Sequelize.STRING,
        allowNull: true
      },
      email: {
        type: Sequelize.STRING,
        allowNull: false,
        unique: true // email unico
      },
      senha_hash: {
        type: Sequelize.STRING,
        allowNull: false
      },
      celular: {
        type: Sequelize.STRING,
        allowNull: true
      },
      data_nascimento: {
        type: Sequelize.DATEONLY,
        allowNull: true
      },
      saldo: {
        type: Sequelize.INTEGER,
        allowNull: true
      },
      ativo: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: true
      },
      confirmou_email: {
        type: Sequelize.BOOLEAN,
        allowNull: true,
        defaultValue: false
      },
      avatar: {
        type: Sequelize.STRING,
        allowNull: true
      },
      avaliacao_media: {
        type: Sequelize.FLOAT,
        allowNull: true
      },
      token_fcm: {
        type: Sequelize.STRING,
        allowNull: true
      },
      codigo_ativacao: {
        type: Sequelize.STRING,
        allowNull: true
      },
      configurou_dados: {
        type: Sequelize.BOOLEAN,
        allowNull: true
      },
      cep: {
        type: Sequelize.STRING,
        allowNull: true
      },
      logradouro: {
        type: Sequelize.STRING,
        allowNull: true
      },
      numero: {
        type: Sequelize.STRING,
        allowNull: true
      },
      complemento: {
        type: Sequelize.STRING,
        allowNull: true
      },
      bairro: {
        type: Sequelize.STRING,
        allowNull: true
      },
      video: {
        type: Sequelize.TEXT,
        allowNull: true
      },
      texto_apresentacao: {
        type: Sequelize.STRING,
        allowNull: true
      },
      texto_curriculo: {
        type: Sequelize.STRING,
        allowNull: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('professor');
  }
};
