module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('aula', 'deleted_at', {
        type: Sequelize.DATE
      })
    ]);
  },

  down: queryInterface => {
    return Promise.all([queryInterface.removeColumn('aula', 'deleted_at')]);
  }
};
