module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('plano', 'quantidade_minuto', {
        type: Sequelize.INTEGER,
        allowNull: false // nao pode ser null
      }),
      queryInterface.addColumn('plano', 'valor', {
        type: Sequelize.FLOAT,
        allowNull: false // nao pode ser null
      })
    ]);
  },

  down: queryInterface => {
    return Promise.all([
      queryInterface.removeColumn('plano', 'quantidade_minuto'),
      queryInterface.removeColumn('plano', 'valor')
    ]);
  }
};
