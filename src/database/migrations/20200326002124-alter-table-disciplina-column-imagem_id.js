module.exports = {
  up: queryInterface => {
    return queryInterface.renameColumn(
      'disciplina',
      'imagem_id',
      'imagemdark_id'
    );
  },

  down: queryInterface => {
    return queryInterface.renameColumn(
      'disciplina',
      'imagemdark_id',
      'imagem_id'
    );
  }
};
