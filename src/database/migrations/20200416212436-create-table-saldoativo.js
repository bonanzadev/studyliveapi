module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('saldoativo', {
      id: {
        type: Sequelize.INTEGER, // tipo inteiro
        allowNull: false, // nao pode ser null
        autoIncrement: true, // auto increment
        primaryKey: true // chave primaria da tabela
      },
      aluno_id: {
        type: Sequelize.INTEGER,
        references: { model: 'aluno', key: 'id' },
        onDelete: 'SET NULL',
        allowNull: false
      },
      minutos: {
        type: Sequelize.INTEGER,
        allowNull: false
      },
      data_inicio: {
        type: Sequelize.DATE
      },
      data_fim: {
        type: Sequelize.DATE
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('saldoativo');
  }
};
