module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('disponibilidade', {
      id: {
        type: Sequelize.INTEGER, // tipo inteiro
        allowNull: false, // nao pode ser null
        autoIncrement: true, // auto increment
        primaryKey: true // chave primaria da tabela
      },
      professor_id: {
        type: Sequelize.INTEGER,
        references: { model: 'professor', key: 'id' },
        onDelete: 'SET NULL',
        allowNull: false
      },
      data: {
        type: Sequelize.DATE,
        allowNull: false
      },
      disponivel: {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('disponibilidade');
  }
};
