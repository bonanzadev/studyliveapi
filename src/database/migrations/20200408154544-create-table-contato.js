module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('faleconosco', {
      id: {
        type: Sequelize.INTEGER, // tipo inteiro
        allowNull: false, // nao pode ser null
        autoIncrement: true, // auto increment
        primaryKey: true // chave primaria da tabela
      },
      professor_id: {
        type: Sequelize.INTEGER,
        references: { model: 'professor', key: 'id' },
        onDelete: 'SET NULL',
        allowNull: true
      },
      aluno_id: {
        type: Sequelize.INTEGER,
        references: { model: 'aluno', key: 'id' },
        onDelete: 'SET NULL',
        allowNull: true
      },
      motivo: {
        type: Sequelize.STRING(100),
        allowNull: false
      },
      email_contato: {
        type: Sequelize.STRING(50),
        allowNull: false
      },
      mensagem: {
        type: Sequelize.STRING(255),
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('faleconosco');
  }
};
