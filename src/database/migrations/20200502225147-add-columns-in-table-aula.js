module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('aula', 'data_inicio_real', {
        type: Sequelize.DATE
      }),
      queryInterface.addColumn('aula', 'data_fim_real', {
        type: Sequelize.DATE
      })
    ]);
  },

  down: queryInterface => {
    return Promise.all([
      queryInterface.removeColumn('aula', 'data_inicio_real'),
      queryInterface.removeColumn('aula', 'data_fim_real')
    ]);
  }
};
