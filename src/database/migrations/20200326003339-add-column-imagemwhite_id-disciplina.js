module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.addColumn('disciplina', 'imagemwhite_id', {
      type: Sequelize.INTEGER,
      references: { model: 'file', key: 'id' },
      onUpdate: 'CASCADE',
      onDelete: 'SET NULL',
      allowNull: true
    });
  },

  down: queryInterface => {
    return queryInterface.removeColumn('disciplina', 'imagemwhite_id');
  }
};
