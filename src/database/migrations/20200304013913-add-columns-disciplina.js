module.exports = {
  up: (queryInterface, Sequelize) => {
    return Promise.all([
      queryInterface.addColumn('disciplina', 'ativo', {
        type: Sequelize.BOOLEAN,
        defaultValue: true
      })
    ]);
  },

  down: queryInterface => {
    return Promise.all([queryInterface.removeColumn('disciplina', 'ativo')]);
  }
};
