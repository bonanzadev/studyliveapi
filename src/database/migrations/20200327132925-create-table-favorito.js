module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('favorito', {
      id: {
        type: Sequelize.INTEGER, // tipo inteiro
        allowNull: false, // nao pode ser null
        autoIncrement: true, // auto increment
        primaryKey: true // chave primaria da tabela
      },
      professor_id: {
        type: Sequelize.INTEGER,
        references: { model: 'professor', key: 'id' },
        onDelete: 'SET NULL',
        allowNull: false
      },
      aluno_id: {
        type: Sequelize.INTEGER,
        references: { model: 'aluno', key: 'id' },
        onDelete: 'SET NULL',
        allowNull: false
      },
      created_at: {
        type: Sequelize.DATE,
        allowNull: false
      },
      updated_at: {
        type: Sequelize.DATE,
        allowNull: false
      }
    });
  },

  down: queryInterface => {
    return queryInterface.dropTable('favorito');
  }
};
