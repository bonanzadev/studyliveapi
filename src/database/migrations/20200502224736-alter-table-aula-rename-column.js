module.exports = {
  up: queryInterface => {
    return Promise.all([
      queryInterface.renameColumn(
        'aula',
        'data_inicio',
        'data_inicio_previsto'
      ),
      queryInterface.renameColumn('aula', 'data_fim', 'data_fim_previsto')
    ]);
  },

  down: queryInterface => {
    return Promise.all([
      queryInterface.renameColumn(
        'aula',
        'data_inicio_previsto',
        'data_inicio'
      ),
      queryInterface.renameColumn('aula', 'data_fim_previsto', 'data_fim')
    ]);
  }
};
