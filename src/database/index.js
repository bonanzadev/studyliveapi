import Sequelize from 'sequelize';

import Aluno from '../app/models/Aluno';
import Professor from '../app/models/Professor';
import Plano from '../app/models/Plano';
import Disciplina from '../app/models/Disciplina';
import Materia from '../app/models/Materia';
import File from '../app/models/File';
import Aula from '../app/models/Aula';
import FaleConosco from '../app/models/FaleConosco';
import Disponibilidade from '../app/models/Disponibilidade';
import SaldoAtivo from '../app/models/SaldoAtivo';

import databaseConfig from '../config/database';

const models = [
  Aluno,
  Professor,
  Plano,
  Disciplina,
  Materia,
  File,
  Aula,
  FaleConosco,
  Disponibilidade,
  SaldoAtivo
];

class Database {
  constructor() {
    this.init();
  }

  init() {
    this.connection = new Sequelize(databaseConfig);
    models
      .map(model => model.init(this.connection))
      .map(model => model.associate && model.associate(this.connection.models));
  }
}

export default new Database();
