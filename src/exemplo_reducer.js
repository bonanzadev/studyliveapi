const dados = [
  {
    nome: 'cerveja',
    categoria: 'bebida'
  },
  {
    nome: 'suco',
    categoria: 'bebida'
  },
  {
    nome: 'joelho',
    categoria: 'salgado'
  },
  {
    nome: 'pastel',
    categoria: 'salgado'
  },
  {
    nome: 'kkkk',
    categoria: 'salgado'
  }
];

const novoArray = dados.reduce(function(prev, curr) {
  prev[curr.categoria] = prev[curr.categoria] || [];
  prev[curr.categoria].push(curr);
  return prev;
}, {});

console.log(novoArray);
