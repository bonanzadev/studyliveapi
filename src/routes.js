import express, { Router } from 'express';
import multer from 'multer';
import cors from 'cors';
import path from 'path';

import multerConfig from './config/multer';
import AlunoController from './app/controllers/AlunoController';
import ProfessorController from './app/controllers/ProfessorController';
import PlanoController from './app/controllers/PlanoController';
import DisciplinaController from './app/controllers/DisciplinaController';
import MateriaController from './app/controllers/MateriaController';
import SessionController from './app/controllers/SessionController';
import FileController from './app/controllers/FileController';
import AulaController from './app/controllers/AulaController';
import FavoritoController from './app/controllers/FavoritoController'; // Controller para aluno favoritar professores
import FaleConoscoController from './app/controllers/FaleConoscoController';
import AvaliacaoController from './app/controllers/AvaliacaoController';
import DisponibilidadeController from './app/controllers/DisponibilidadeController';
import ChatController from './app/controllers/ChatController';
import ContratacaoController from './app/controllers/ContratacaoController';
import SaldoAtivoController from './app/controllers/SaldoAtivoController';

import authMiddleware from './app/middlewares/auth';

const routes = new Router();
const upload = multer(multerConfig);
routes.use(cors());

routes.use(
  '/img',
  express.static(path.resolve(__dirname, '..', 'temp', 'uploads', 'img'))
);

routes.post('/aluno', AlunoController.store); // Cadastro de Aluno
routes.post('/professor', ProfessorController.store); // Cadastro de Professor
routes.post('/sessions', SessionController.store); // Criacao de Session
routes.post('/faleconosco', FaleConoscoController.store); // fale conosco
/**
 * se passar pela autenticacao, eh continuado o fluxo das rotas
 */
routes.use(authMiddleware);

/**
 * Inicio rotas plano
 */
routes.get('/plano', PlanoController.index); // Listagem de todos os planos
routes.get('/plano/:id', PlanoController.show); // Listagem de um plano
routes.post('/plano', PlanoController.store); // Criacao de Plano
routes.put('/plano/:id', PlanoController.update); // alteracao de Plano

/**
 * Inicio rotas disciplina
 */
routes.post('/disciplina', DisciplinaController.store); // Criacao de disciplina
routes.put('/disciplina/:id', DisciplinaController.update); // alteracao de disciplina
routes.get('/disciplina/:id?', DisciplinaController.index); // listando disciplina(s)

/**
 * Inicio rotas materia
 */
routes.post('/materia', MateriaController.index); // listar varias materias
routes.get('/materia/:id', MateriaController.show); // listar uma materia
routes.post('/materia', MateriaController.store); // Criacao de materia
routes.put('/materia/:id', MateriaController.update); // alteracao de materia

/**
 * Inicio rotas aluno
 */
routes.get('/aluno', AlunoController.index); // listar todos alunos
routes.get('/aluno/:id', AlunoController.show); // listar um aluno
routes.put('/aluno', AlunoController.update); // alteracao de dados do aluno
routes.get('/aluno/:id/saldoativo', SaldoAtivoController.show); // listar o saldo ativo do aluno

/**
 * Inicio rotas professor
 */
routes.post('/professor/filter', ProfessorController.index); // listar varios professores
routes.get('/professor/:id', ProfessorController.show); // listar um professor
routes.put('/professor', ProfessorController.update); // alteracao de dados do professor
routes.post('/professor/disponibilidade', DisponibilidadeController.store); // alteracao de dados do professor
routes.get('/professor/:id/disponibilidade', DisponibilidadeController.index); // alteracao de dados do professor

/**
 * Inicio rotas aula
 */
routes.post('/aula', AulaController.store); // criação de dados da aula
routes.get('/aula/:id?', AulaController.index); // listagem de dados da aula
routes.delete('/aula/:id', AulaController.delete); // listagem de dados da aula

/**
 * Inicio rotas aula
 */
routes.get('/favorito', FavoritoController.index); // listar todos os professores favoritos
routes.post('/favorito/:id', FavoritoController.store); // criação de dados da aula
routes.delete('/favorito/:id', FavoritoController.delete); // deletar professor dos favoritos

/**
 * Inicio rotas avaliacao
 */
routes.post('/avaliacao/:id', AvaliacaoController.store); // criação da avaliacao
routes.post('/avaliacao', AvaliacaoController.index); // listagem das avaliacoes

/**
 * Inicio rotas avaliacao
 */
routes.post('/chat', ChatController.index); // listagem das conversas

/**
 * Inicio rotas Contratacao
 */
routes.post('/contratacao', ContratacaoController.store); // criando contratacao
routes.delete('/contratacao', ContratacaoController.delete); // cancelando plano

routes.post('/file', upload.single('file'), FileController.store);
export default routes;
