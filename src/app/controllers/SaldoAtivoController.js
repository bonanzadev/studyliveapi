import SaldoAtivo from '../models/SaldoAtivo';

class SaldoAtivoController {
  async show(req, res) {
    try {
      const saldoativo = await SaldoAtivo.findOne({
        where: { aluno_id: req.params.id },
        attributes: {
          exclude: ['createdAt', 'updatedAt']
        }
      });
      return res.json(saldoativo);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new SaldoAtivoController();
