import Aluno from '../models/Aluno';
import Professor from '../models/Professor';
import Aula from '../models/Aula';

class AvaliacaoController {
  async index(req, res) {
    const whereStatement = {};
    whereStatement.aconteceu = true;
    if (req.body.aluno_id) {
      whereStatement.aluno_id = req.body.aluno_id;
    } else if (req.body.professor_id) {
      whereStatement.professor_id = req.body.professor_id;
    } else {
      return res.status(200).json({ error: 'Informe um aluno ou professor!' });
    }
    try {
      const avaliacoes = await Aula.findAll({
        where: whereStatement,
        limit: 10,
        order: [['id', 'DESC']],
        attributes: [
          'id',
          'avaliacao_aluno',
          'avaliacao_aluno_mensagem',
          'avaliacao_professor',
          'avaliacao_professor_mensagem'
        ],
        include: [
          {
            model: Aluno,
            as: 'aluno',
            attributes: ['id', 'nome', 'apelido']
          },
          {
            model: Professor,
            as: 'professor',
            attributes: ['id', 'nome', 'apelido']
          }
        ]
      });
      return res.status(200).json({ avaliacoes });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async store(req, res) {
    const {
      avaliacao_professor,
      avaliacao_professor_mensagem,
      avaliacao_aluno,
      avaliacao_aluno_mensagem
    } = req.body;
    if (
      !req.params.id ||
      req.params.id === undefined ||
      req.params.id === null
    ) {
      return res
        .status(200)
        .json({ error: 'Informa a aula que deseja avaliar!' });
    }
    try {
      const aula = await Aula.findByPk(req.params.id); // find by primary key
      if (!aula.aconteceu) {
        return res.status(200).json({
          error: 'Você não pode avaliar uma aula que ainda não ocorreu!'
        });
      }
      if (req.tipo_usuario === 'aluno') {
        if (aula.aluno_id !== req.usuarioId) {
          return res.status(200).json({
            error: 'Você não pode avaliar uma aula que não lhe pertence!'
          });
        }
        await aula.update({
          avaliacao_professor,
          avaliacao_professor_mensagem
        });
      } else if (req.tipo_usuario === 'professor') {
        if (aula.professor_id !== req.usuarioId) {
          return res.status(200).json({
            error: 'Você não pode avaliar uma aula que não lhe pertence!'
          });
        }
        await aula.update({
          avaliacao_aluno,
          avaliacao_aluno_mensagem
        });
      }
      return res.status(200).json({ success: true });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new AvaliacaoController();
