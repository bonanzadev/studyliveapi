import File from '../models/File';

class FileController {
  async store(req, res) {
    const { originalname: nome, filename: path } = req.file;

    const { id } = await File.create({
      nome,
      path
    });

    return res.json({ id, nome, path });
  }
}

export default new FileController();
