import * as Yup from 'yup';
import Professor from '../models/Professor';
import Aluno from '../models/Aluno';
import File from '../models/File';
import Disciplina from '../models/Disciplina';
import Materia from '../models/Materia';

import CadastroProfessorMail from '../jobs/CadastroProfessorMail';
import Queue from '../../lib/Queue';

class ProfessorController {
  async index(req, res) {
    const whereStatementMateria = {};
    const whereStatementDisciplina = {};
    if (req.body.materias !== undefined)
      whereStatementMateria.id = req.body.materias;
    if (req.body.disciplinas !== undefined)
      whereStatementDisciplina.id = req.body.disciplinas;

    const aluno = await Aluno.findByPk(req.usuarioId); // find by primary key
    const favoritos = await aluno.getProfessores_favoritos({
      attributes: {
        exclude: ['senha_hash', 'createdAt', 'updatedAt']
      }
    });
    const professores = await Professor.findAll({
      where: { ativo: true, confirmou_email: true, configurou_dados: true },
      attributes: ['id'],
      include: [
        {
          model: Materia,
          as: 'materias',
          through: { attributes: [] }, // nao retornar dados da tabela pivot
          attributes: [],
          where: whereStatementMateria,
          include: [
            {
              model: Disciplina,
              as: 'disciplina',
              attributes: [],
              where: whereStatementDisciplina
            }
          ]
        }
      ]
    });
    const professorReturn = { professores: [] };

    const promises = professores.map(async professor => {
      await Professor.findOne({
        where: { id: professor.id },
        attributes: [
          'id',
          'nome',
          'apelido',
          'video',
          'email',
          'celular',
          'data_nascimento',
          'avaliacao_media'
        ],
        include: [
          {
            model: File,
            as: 'avatar',
            attributes: ['id', 'path']
          },
          {
            model: Materia,
            as: 'materias',
            through: { attributes: [] }, // nao retornar dados da tabela pivot
            attributes: ['id', 'nome'],
            include: [
              {
                model: Disciplina,
                as: 'disciplina',
                attributes: ['id', 'nome'],
                include: [
                  {
                    model: File,
                    as: 'thumb_white',
                    attributes: {
                      exclude: ['createdAt', 'updatedAt']
                    }
                  },
                  {
                    model: File,
                    as: 'thumb_dark',
                    attributes: {
                      exclude: ['createdAt', 'updatedAt']
                    }
                  }
                ]
              }
            ]
          }
        ]
      }).then(p => {
        const { id, nome, avatar, video, avaliacao_media, materias } = p;
        const professorFavorito = favoritos.find(prof => prof.id === id);

        professorReturn.professores.push({
          id,
          nome,
          avatar,
          video,
          avaliacao_media,
          qtd_aula: 20,
          favorito: !!professorFavorito,
          materias
        });
      });
    });
    await Promise.all(promises);
    return res.json(professorReturn);
  }

  async show(req, res) {
    try {
      const aluno = await Aluno.findByPk(req.usuarioId); // find by primary key
      const favoritos = await aluno.getProfessores_favoritos({
        attributes: {
          exclude: ['senha_hash', 'createdAt', 'updatedAt']
        }
      });
      const professorFavorito = await favoritos.find(
        prof => prof.id === parseInt(req.params.id, 10)
      );
      const professor = await Professor.findAll({
        where: {
          id: req.params.id,
          ativo: true,
          confirmou_email: true,
          configurou_dados: true
        },
        attributes: {
          exclude: ['senha_hash', 'createdAt', 'updatedAt', 'avatar_id']
        },
        include: [
          {
            model: File,
            as: 'avatar',
            attributes: ['id', 'path']
          },
          {
            model: Materia,
            as: 'materias',
            through: { attributes: [] }, // nao retornar dados da tabela pivot
            attributes: {
              exclude: ['createdAt', 'updatedAt', 'disciplina_id']
            },
            include: [
              {
                model: Disciplina,
                as: 'disciplina',
                attributes: {
                  exclude: [
                    'createdAt',
                    'updatedAt',
                    'imagemdark_id',
                    'imagemwhite_id'
                  ]
                },
                include: [
                  {
                    model: File,
                    as: 'thumb_white',
                    attributes: {
                      exclude: ['createdAt', 'updatedAt']
                    }
                  },
                  {
                    model: File,
                    as: 'thumb_dark',
                    attributes: {
                      exclude: ['createdAt', 'updatedAt']
                    }
                  }
                ]
              }
            ]
          }
        ]
      });
      professor[0].dataValues.favorito = !!professorFavorito;
      return res.status(200).json(professor);
    } catch (error) {
      return res
        .status(500)
        .json({ error: `Ops... estamos passando por uma instabilidade.` });
    }
  }

  async store(req, res) {
    // criacao de professor
    const schema = Yup.object().shape({
      nome: Yup.string().required(() => {
        return res.status(400).json({ error: 'Preencha o campo nome!' });
      }),
      apelido: Yup.string(),
      email: Yup.string()
        .email()
        .required(() => {
          return res.status(400).json({ error: 'Preencha o campo email!' });
        }),
      data_nascimento: Yup.date().required(() => {
        return res
          .status(400)
          .json({ error: 'Preencha o campo data de nascimento!' });
      }),
      celular: Yup.string().required(() => {
        return res.status(400).json({ error: 'Preencha o campo celular!' });
      }),
      senha: Yup.string()
        .required(() => {
          return res.status(400).json({ error: 'Preencha o campo senha!' });
        })
        .min(6, () => {
          return res
            .status(400)
            .json({ error: 'A senha deve possuir no minimo 6 caracteres.' });
        })
    });

    /**
     * De a cordo com as regras a cima
     * validamos os dados com o yupi
     */
    if (!(await schema.isValid(req.body))) {
      return res
        .status(400)
        .json({ error: 'Verifique os campos obrigátorios!' });
    }

    try {
      const professorExiste = await Professor.findOne({
        where: { email: req.body.email }
      });

      // verifico se o professor ja nao existe
      if (professorExiste) {
        return res.status(400).json({ error: 'Professor já existente!' });
      }
      const { id, nome, apelido, email, celular } = await Professor.create(
        req.body
      );

      await Queue.add(CadastroProfessorMail.key, {
        nome,
        email
      });

      return res.json({
        id,
        nome,
        apelido,
        email,
        celular
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async update(req, res) {
    // atualizacao de dados do professor
    const schema = Yup.object().shape({
      nome: Yup.string(),
      apelido: Yup.string(),
      email: Yup.string().email(),
      celular: Yup.string(),
      senhaAntiga: Yup.string().min(6),
      senha: Yup.string()
        .min(6)
        .when('senhaAntiga', (senhaAntiga, field) =>
          senhaAntiga ? field.required() : field
        ),
      confirmaSenha: Yup.string().when('senha', (senha, field) =>
        senha ? field.required().oneOf([Yup.ref('senha')]) : field
      )
    });

    if (!(await schema.isValid(req.body))) {
      return res
        .status(400)
        .json({ error: 'Verifique os campos obrigatorios!' });
    }
    try {
      const { email, senhaAntiga } = req.body;

      const professor = await Professor.findByPk(req.usuarioId); // find by primary key

      if (email && email !== professor.email) {
        const professorExiste = await Professor.findOne({
          where: { email }
        });
        if (professorExiste) {
          return res.status(400).json({ error: 'Professor já existente!' });
        }
      }

      if (senhaAntiga && !(await professor.verificaSenha(senhaAntiga))) {
        return res.status(401).json({ error: 'Senha não corresponde!' });
      }
      const { id } = await professor.update(req.body);
      if (req.body.materias !== undefined)
        await professor.setMaterias(req.body.materias);

      const professorReturn = await Professor.findAll({
        where: { id },
        attributes: [
          'id',
          'nome',
          'apelido',
          'email',
          'celular',
          'data_nascimento'
        ],
        include: [
          {
            model: File,
            as: 'avatar',
            attributes: ['id', 'path']
          },
          {
            model: Materia,
            as: 'materias',
            through: { attributes: [] }, // nao retornar dados da tabela pivot
            attributes: ['id', 'nome'],
            include: [
              {
                model: Disciplina,
                as: 'disciplina',
                attributes: ['id', 'nome']
              }
            ]
          }
        ]
      });

      return res.json(professorReturn);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new ProfessorController();
