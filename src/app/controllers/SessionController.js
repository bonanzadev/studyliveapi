import * as Yup from 'yup';
import jwt from 'jsonwebtoken';

import Aluno from '../models/Aluno';
import Professor from '../models/Professor';
import File from '../models/File';
import Plano from '../models/Plano';
import SaldoAtivo from '../models/SaldoAtivo';
import authConfig from '../../config/auth';

class SessionController {
  async store(req, res) {
    const schema = Yup.object().shape({
      email: Yup.string()
        .email()
        .required(() => {
          res.status(200).json({ error: 'informe um email!' });
        }),
      tipo_usuario: Yup.string().required(() => {
        res.status(200).json({ error: 'informe o tipo de usuário!' });
      }),
      senha: Yup.string().required(() => {
        res.status(200).json({ error: 'informe a senha!' });
      })
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(200).json({ error: 'Falha nas validações!' });
    }
    try {
      const { email, senha, tipo_usuario } = req.body;
      let usuario = null;
      if (tipo_usuario === 'aluno') {
        usuario = await Aluno.findOne({
          where: { email },
          include: [
            {
              model: File,
              as: 'avatar',
              attributes: ['id', 'path']
            },
            {
              model: Plano,
              as: 'plano',
              attributes: ['id', 'descricao']
            }
          ]
        });
        const saldo_ativo = await SaldoAtivo.findOne({
          where: { aluno_id: usuario.id },
          attributes: {
            exclude: ['createdAt', 'updatedAt', 'aluno_id']
          }
        });
        if (!usuario) {
          return res.status(200).json({ error: 'Usuário não encontrado!' });
        }

        if (!(await usuario.verificaSenha(senha))) {
          return res.status(200).json({ error: 'Senha incorreta!' });
        }

        const {
          id,
          nome,
          apelido,
          celular,
          data_nascimento,
          confirmou_email,
          avatar,
          plano,
          codigo_indicacao
        } = usuario;

        return res.json({
          usuario: {
            id,
            nome,
            email,
            apelido,
            celular,
            data_nascimento,
            confirmou_email,
            avatar,
            plano,
            codigo_indicacao,
            saldo_ativo
          },
          token: jwt.sign({ id, tipo_usuario }, authConfig.secret, {
            expiresIn: authConfig.expiresIn
          })
          /**
           * o primeiro parametro eh um objeto payload do jwt
           * o segundo parametro eh uma string unica entre todas as aplicacoes do mundo kk
           * o terceiro parametro sao algumas configuracoes para este token. todo token jwt
           * tem obrigatoriamente uma validade.
           */
        });
      }
      usuario = await Professor.findOne({
        where: { email },
        include: [
          {
            model: File,
            as: 'avatar',
            attributes: ['id', 'path']
          }
        ]
      });

      if (!usuario) {
        return res.status(200).json({ error: 'Usuário não encontrado!' });
      }

      if (!(await usuario.verificaSenha(senha))) {
        return res.status(200).json({ error: 'Senha incorreta!' });
      }

      const {
        id,
        nome,
        apelido,
        celular,
        data_nascimento,
        confirmou_email,
        avatar,
        plano,
        codigo_indicacao
      } = usuario;

      return res.json({
        usuario: {
          id,
          nome,
          email,
          apelido,
          celular,
          data_nascimento,
          confirmou_email,
          avatar,
          plano,
          codigo_indicacao
        },
        token: jwt.sign({ id, tipo_usuario }, authConfig.secret, {
          expiresIn: authConfig.expiresIn
        })
        /**
         * o primeiro parametro eh um objeto payload do jwt
         * o segundo parametro eh uma string unica entre todas as aplicacoes do mundo kk
         * o terceiro parametro sao algumas configuracoes para este token. todo token jwt
         * tem obrigatoriamente uma validade.
         */
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new SessionController();
