import * as Yup from 'yup';
import crypto from 'crypto';
import Aluno from '../models/Aluno';
import Plano from '../models/Plano';
import File from '../models/File';

import CadastroAlunoMail from '../jobs/CadastroAlunoMail';
import Queue from '../../lib/Queue';

class AlunoController {
  async index(req, res) {
    const whereStatement = {};
    if (req.body.codigo_indicacao)
      whereStatement.codigo_indicacao = req.body.codigo_indicacao;
    if (req.body.email) whereStatement.email = req.body.email;
    if (req.body.celular) whereStatement.celular = req.body.celular;
    const aluno = await Aluno.findAll({
      where: whereStatement,
      attributes: [
        'id',
        'nome',
        'apelido',
        'codigo_indicacao',
        'email',
        'celular',
        'data_nascimento'
      ],
      include: [
        {
          model: Plano,
          as: 'plano',
          attributes: ['id', 'descricao']
        }
      ]
    });
    return res.json(aluno);
  }

  async show(req, res) {
    const aluno = await Aluno.findAll({
      where: { id: req.params.id },
      attributes: [
        'id',
        'nome',
        'apelido',
        'codigo_indicacao',
        'email',
        'celular',
        'data_nascimento'
      ],
      include: [
        {
          model: Plano,
          as: 'plano',
          attributes: ['id', 'descricao']
        }
      ]
    });
    return res.json(aluno);
  }

  async store(req, res) {
    // criacao de aluno
    const schema = Yup.object().shape({
      nome: Yup.string().required(() => {
        return res.status(200).json({ error: 'Preencha o campo nome!' });
      }),
      apelido: Yup.string(),
      email: Yup.string()
        .email()
        .required(() => {
          return res.status(200).json({ error: 'Preencha o campo email!' });
        }),
      data_nascimento: Yup.date().required(() => {
        return res
          .status(200)
          .json({ error: 'Preencha o campo data de nascimento!' });
      }),
      celular: Yup.string().required(() => {
        return res.status(200).json({ error: 'Preencha o campo celular!' });
      }),
      senha: Yup.string()
        .required(() => {
          return res.status(200).json({ error: 'Preencha o campo senha!' });
        })
        .min(6, () => {
          return res
            .status(200)
            .json({ error: 'A senha deve possuir no minimo 6 caracteres.' });
        })
    });

    /**
     * De a cordo com as regras a cima
     * validamos os dados com o yupi
     */
    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: 'Verifique os campos obrigatórios!' });
    }
    try {
      const alunoExiste = await Aluno.findOne({
        where: { email: req.body.email }
      });
      // verifico se o aluno ja nao existe
      if (alunoExiste) {
        return res.status(200).json({ error: 'Usuário já existente!' });
      }
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }

    try {
      req.body.codigo_indicacao = crypto.randomBytes(3).toString('hex');
      const {
        id,
        nome,
        apelido,
        email,
        celular,
        codigo_indicacao
      } = await Aluno.create(req.body);

      await Queue.add(CadastroAlunoMail.key, {
        nome,
        email
      });

      return res.json({
        id,
        nome,
        apelido,
        email,
        celular,
        codigo_indicacao
      });
    } catch (error) {
      console.log(error);
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async update(req, res) {
    // atualizacao de dados do aluno
    const schema = Yup.object().shape({
      nome: Yup.string(),
      apelido: Yup.string(),
      email: Yup.string().email(),
      celular: Yup.string(),
      senhaAntiga: Yup.string().min(6),
      senha: Yup.string()
        .min(6)
        .when('senhaAntiga', (senhaAntiga, field) =>
          senhaAntiga
            ? field.required(() => {
                return res
                  .status(200)
                  .json({ error: 'Preencha o campo senha!' });
              })
            : field
        ),
      confirmaSenha: Yup.string().when('senha', (senha, field) =>
        senha
          ? field
              .required(() => {
                return res.status(200).json({
                  error: 'Preencha o campo confirmando a nova senha!'
                });
              })
              .oneOf([Yup.ref('senha')])
          : field
      )
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(200).json({ error: 'As senhas não iguais!' });
    }
    try {
      const { emailReq, senhaAntiga } = req.body;

      const aluno = await Aluno.findByPk(req.usuarioId); // find by primary key

      if (emailReq && emailReq !== aluno.email) {
        const alunoExiste = await Aluno.findOne({
          where: { emailReq }
        });
        if (alunoExiste) {
          return res.status(200).json({ error: 'Usuário já existente!' });
        }
      }

      if (senhaAntiga && !(await aluno.verificaSenha(senhaAntiga))) {
        return res.status(200).json({ error: 'Senha não corresponde!' });
      }

      const { id } = await aluno.update(req.body);

      const alunoReturn = await Aluno.findAll({
        where: { id },
        attributes: [
          'id',
          'nome',
          'apelido',
          'email',
          'celular',
          'data_nascimento'
        ],
        include: [
          {
            model: Plano,
            as: 'plano',
            attributes: ['id', 'descricao']
          },
          {
            model: File,
            as: 'avatar',
            attributes: ['id', 'path']
          }
        ]
      });
      return res.json(alunoReturn);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new AlunoController();
