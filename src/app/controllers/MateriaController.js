import * as Yup from 'yup';
import Materia from '../models/Materia';
import Disciplina from '../models/Disciplina';

class MateriaController {
  async index(req, res) {
    const whereStatementDisciplina = {};

    if (req.body.disciplina_id)
      whereStatementDisciplina.id = req.body.disciplina_id;
    const materia = await Materia.findAll({
      where: {},
      attributes: ['id', 'nome'],
      include: [
        {
          model: Disciplina,
          as: 'disciplina',
          attributes: ['id', 'nome'],
          where: whereStatementDisciplina
        }
      ]
    });
    return res.status(200).json(materia);
  }

  async show(req, res) {
    const materia = await Materia.findAll({
      where: { id: req.params.id },
      attributes: ['id', 'nome'],
      include: [
        {
          model: Disciplina,
          as: 'disciplina',
          attributes: ['id', 'nome']
        }
      ]
    });
    return res.status(200).json(materia);
  }

  async store(req, res) {
    // criacao de materia
    const schema = Yup.object().shape({
      disciplina_id: Yup.string().required(() => {
        return res.status(200).json({ error: 'Preencha o campo Disciplina!' });
      }),
      nome: Yup.string().required(() => {
        return res.status(200).json({ error: 'Preencha o campo Nome!' });
      })
    });

    /**
     * De a cordo com as regras a cima
     * validamos os dados com o yupi
     */
    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: 'Verifique os campos obrigatórios!' });
    }
    try {
      const disciplinaExiste = await Disciplina.findByPk(
        req.body.disciplina_id
      );
      if (!disciplinaExiste) {
        return res.status(200).json({ error: 'Disciplina inválida!' });
      }

      const { id, disciplina_id, nome } = await Materia.create(req.body);

      return res.json({
        id,
        disciplina_id,
        nome
      });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }

  async update(req, res) {
    // atualizacao de dados do materia
    const schema = Yup.object().shape({
      disciplina_id: Yup.string(),
      nome: Yup.string(),
      descricao: Yup.string()
    });

    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: 'Verifique os campos obrigatórios!' });
    }
    try {
      if (req.body.disciplina_id) {
        const disciplinaExiste = await Disciplina.findByPk(
          req.body.disciplina_id
        );
        if (disciplinaExiste) {
          return res.status(200).json({ error: 'Disciplina inválida!' });
        }
      }
      const materia = await Materia.findByPk(req.params.id); // find by primary key

      const { id, nome } = await materia.update(req.body);

      return res.json({
        id,
        nome
      });
    } catch (error) {
      return res.status(500).json({ error });
    }
  }
}

export default new MateriaController();
