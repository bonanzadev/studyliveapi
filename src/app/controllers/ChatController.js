import { Op } from 'sequelize';
import Aluno from '../models/Aluno';
import Professor from '../models/Professor';
import File from '../models/File';

class ChatController {
  async index(req, res) {
    let usuario = null;
    if (req.tipo_usuario === 'aluno') {
      usuario = Professor;
    } else if (req.tipo_usuario === 'professor') {
      usuario = Aluno;
    } else {
      return res.status(200).json({ error: 'Informe um aluno ou professor!' });
    }

    if (!req.body.ids)
      return res.status(200).json({ error: 'Informe o(s) id(s) que deseja!' });
    try {
      const conversas = await usuario.findAll({
        where: {
          id: {
            [Op.in]: req.body.ids
          }
        },
        limit: 10,
        attributes: ['id', 'nome', 'apelido', 'email'],
        include: [
          {
            model: File,
            as: 'avatar',
            attributes: ['id', 'nome', 'path']
          }
        ]
      });
      return res.status(200).json({ conversas });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new ChatController();
