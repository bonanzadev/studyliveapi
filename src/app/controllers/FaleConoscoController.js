import * as Yup from 'yup';
import FaleConosco from '../models/FaleConosco';

class FaleConoscoController {
  async store(req, res) {
    const schema = Yup.object().shape({
      motivo: Yup.string().required(() => {
        res.status(200).json({ error: 'informe o motivo do contato!' });
      })
    });

    if (!(await schema.isValid(req.body))) {
      return res.status(200).json({ error: 'Falha nas validações!' });
    }
    try {
      const { id } = await FaleConosco.create(req.body);

      return res.json({
        id
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new FaleConoscoController();
