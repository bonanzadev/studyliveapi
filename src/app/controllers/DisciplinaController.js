import * as Yup from 'yup';
import Disciplina from '../models/Disciplina';
import File from '../models/File';

class DisciplinaController {
  async index(req, res) {
    const whereStatement = {};
    if (req.params.id) whereStatement.id = req.params.id;
    const disciplina = await Disciplina.findAll({
      where: whereStatement,
      attributes: ['id', 'nome', 'descricao', 'ativo'],
      include: [
        {
          model: File,
          as: 'thumb_dark',
          attributes: ['id', 'path']
        },
        {
          model: File,
          as: 'thumb_white',
          attributes: ['id', 'path']
        }
      ]
    });
    return res.status(200).json(disciplina);
  }

  async store(req, res) {
    // criacao de plano
    const schema = Yup.object().shape({
      nome: Yup.string().required(() => {
        return res.status(200).json({ error: 'Preencha o campo nome!' });
      }),
      imagem_id: Yup.string().required(() => {
        return res.status(200).json({ error: 'Preencha o campo imagem_id!' });
      })
    });

    /**
     * De a cordo com as regras a cima
     * validamos os dados com o yupi
     */
    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: 'Verifique os campos obrigatórios!' });
    }
    try {
      const { id } = await Disciplina.create(req.body);
      const disciplinaReturn = await Disciplina.findAll({
        where: { id },
        attributes: ['id', 'nome', 'descricao'],
        include: [
          {
            model: File,
            as: 'thumb_dark',
            attributes: ['id', 'path']
          },
          {
            model: File,
            as: 'thumb_white',
            attributes: ['id', 'path']
          }
        ]
      });
      return res.json(disciplinaReturn);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async update(req, res) {
    // atualizacao de dados do plano
    const schema = Yup.object().shape({
      descricao: Yup.string()
    });

    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: 'Verifique os campos obrigatórios!' });
    }
    try {
      const disciplina = await Disciplina.findByPk(req.params.id); // find by primary key

      const { id } = await disciplina.update(req.body);
      const disciplinaReturn = await Disciplina.findAll({
        where: { id },
        attributes: ['id', 'nome', 'descricao'],
        include: [
          {
            model: File,
            as: 'thumb_dark',
            attributes: ['id', 'path']
          },
          {
            model: File,
            as: 'thumb_white',
            attributes: ['id', 'path']
          }
        ]
      });
      return res.json(disciplinaReturn);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new DisciplinaController();
