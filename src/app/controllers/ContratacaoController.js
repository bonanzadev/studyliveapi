import { formatISO, addMonths } from 'date-fns';
import SaldoAtivo from '../models/SaldoAtivo';
import Plano from '../models/Plano';
import Aluno from '../models/Aluno';

class ContratacaoController {
  async store(req, res) {
    const plano = await Plano.findByPk(req.body.plano_id); // find by primary key
    const aluno = await Aluno.findByPk(req.usuarioId); // find by primary key
    if (!plano)
      return res.status(200).json({ error: 'Este plano não existe!' });
    if (!aluno)
      return res.status(200).json({ error: 'Este aluno não existe!' });
    if (plano.id === aluno.plano_id)
      return res.status(200).json({ error: 'Você já contratou este plano!' });
    const saldoativoExists = await SaldoAtivo.findOne({
      where: { aluno_id: aluno.id }
    });
    const data_inicio = formatISO(new Date());
    const data_fim = formatISO(addMonths(new Date(), 1));
    let contratacao;
    if (plano.experimental) {
      if (saldoativoExists || aluno.plano_id)
        return res
          .status(200)
          .json({ error: 'Você não pode mais contratar a aula experimental!' });

      // fazer pagamento do gateway aqui

      // se for aprovado o pagamento, adiciona saldo
      contratacao = await SaldoAtivo.create({
        aluno_id: aluno.id,
        minutos: plano.quantidade_minuto,
        data_inicio,
        data_fim
      });

      return res.status(200).json(contratacao);
    }
    // fazer pagamento do gateway aqui
    // se for aprovado o pagamento, adiciona saldo

    if (aluno.plano_id && saldoativoExists) {
      const minutosAtivo = saldoativoExists.minutos;
      contratacao = await saldoativoExists.update({
        minutos: minutosAtivo + plano.quantidade_minuto,
        data_inicio,
        data_fim
      });
      aluno.update({
        plano_id: plano.id
      });
      return res.status(200).json(contratacao);
    }
    aluno.update({
      plano_id: plano.id
    });
    contratacao = await SaldoAtivo.create({
      aluno_id: aluno.id,
      minutos: plano.quantidade_minuto,
      data_inicio,
      data_fim
    });

    return res.status(200).json(contratacao);
  }

  async delete(req, res) {
    try {
      const aluno = await Aluno.findByPk(req.usuarioId);
      aluno.plano_id = null;
      aluno.save();
      return res.json({ success: `Plano cancelado com sucesso!` });
    } catch (error) {
      return res
        .status(500)
        .json({ error: `Ops... Não foi possível cancelar o plano.` });
    }
  }
}

export default new ContratacaoController();
