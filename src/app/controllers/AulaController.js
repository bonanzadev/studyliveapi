import * as Yup from 'yup';
import { parseISO, isBefore } from 'date-fns';
import { Op } from 'sequelize';
import Aula from '../models/Aula';
import Aluno from '../models/Aluno';
import Professor from '../models/Professor';
import Disponibilidade from '../models/Disponibilidade';
import File from '../models/File';

class AulaController {
  async index(req, res) {
    const whereStatement = {};
    if (req.params.id) whereStatement.id = req.params.id;
    if (req.tipo_usuario === 'aluno') {
      whereStatement.aluno_id = req.usuarioId;
    } else if (req.tipo_usuario === 'professor') {
      whereStatement.professor_id = req.usuarioId;
    }

    if (req.body.aconteceu !== undefined) whereStatement.aconteceu = req.body.aconteceu;
    const aula = await Aula.findAll({
      where: whereStatement,
      attributes: {
        exclude: ['createdAt', 'updatedAt', 'professor_id', 'aluno_id']
      },
      include: [
        {
          model: Aluno,
          as: 'aluno',
          attributes: {
            exclude: ['senha_hash', 'createdAt', 'updatedAt', 'avatar_id']
          },
          include: [
            {
              model: File,
              as: 'avatar',
              attributes: {
                exclude: ['createdAt', 'updatedAt']
              }
            }
          ]
        },
        {
          model: Professor,
          as: 'professor',
          attributes: {
            exclude: ['senha_hash', 'createdAt', 'updatedAt', 'avatar_id']
          },
          include: [
            {
              model: File,
              as: 'avatar',
              attributes: {
                exclude: ['createdAt', 'updatedAt']
              }
            }
          ]
        }
      ]
    });
    return res.status(200).json(aula);
  }

  async store(req, res) {
    // criacao de aula
    if (
      !req.usuarioId ||
      req.usuarioId === undefined ||
      req.usuarioId === null
    ) {
      return res.status(200).json({ error: `Informe o aluno!` });
    }

    const schema = Yup.object().shape({
      professor_id: Yup.number().required(() => {
        return res
          .status(200)
          .json({ error: `Preencha o campo "professor_id"!` });
      }),
      agendada: Yup.boolean().required(() => {
        return res.status(200).json({ error: `Preencha o campo "agendada"!` });
      }),
      data_inicio_previsto: Yup.date().required(() => {
        return res
          .status(200)
          .json({ error: `Preencha o campo "data_inicio_previsto"!` });
      }),
      data_fim_previsto: Yup.date().when('agendada', (agendada, field) =>
        agendada === true
          ? field.required(() => {
              return res
                .status(200)
                .json({ error: `Preencha o campo "data_fim_previsto"!` });
            })
          : field
      )
    });

    /**
     * De a cordo com as regras a cima
     * validamos os dados com o yupi
     */
    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: 'Verifique os campos obrigatórios!' });
    }
    try {
      const professorExiste = await Professor.findByPk(req.body.professor_id); // find by primary key
      if (!professorExiste) {
        return res.status(200).json({ error: `Professor inexistente!` });
      }
      const alunoExiste = await Aluno.findByPk(req.usuarioId); // find by primary key
      if (!alunoExiste) {
        return res.status(200).json({ error: `Aluno inexistente!` });
      }

      const hora_inicio = parseISO(req.body.data_inicio_previsto); // passando a data para formato js
      const hora_fim = parseISO(req.body.data_fim_previsto); // passando a data para formato js
      const data_atual = new Date();
      /**
       * verificando se data informada
       * esta no passado
       */
      if (isBefore(hora_inicio, data_atual)) {
        return res.status(200).json({ error: `Selecione um data no futuro!` });
      }

      const verificaDisponibilidade = await Aula.findOne({
        where: {
          aconteceu: false,
          [Op.or]: [
            { aluno_id: req.usuarioId },
            { professor_id: req.body.professor_id }
          ],
          [Op.or]: [
            {
              [Op.and]: [
                {
                  data_inicio_previsto: {
                    [Op.gt]: hora_inicio // > x
                  }
                },
                {
                  data_inicio_previsto: {
                    [Op.lt]: hora_fim // > x
                  }
                }
              ]
            },
            {
              [Op.and]: [
                {
                  data_fim_previsto: {
                    [Op.gt]: hora_inicio // > x
                  }
                },
                {
                  data_fim_previsto: {
                    [Op.lt]: hora_fim // > x
                  }
                }
              ]
            },
            {
              [Op.and]: [
                {
                  data_inicio_previsto: {
                    [Op.eq]: hora_inicio // = x
                  }
                },
                {
                  data_fim_previsto: {
                    [Op.eq]: hora_fim // = x
                  }
                }
              ]
            }
          ]
        }
      });
      if (verificaDisponibilidade) {
        return res
          .status(200)
          .json({ error: `Já existe uma aula neste horario!` });
      }
      // return res.status(200).json({ ok: 'ok' });

      req.body.aluno_id = req.usuarioId;
      const { id } = await Aula.create(req.body);
      const aulaReturn = await Aula.findAll({
        where: { id },
        attributes: [
          'id',
          'data_inicio_previsto',
          'data_fim_previsto',
          'channel',
          'agendada'
        ],
        include: [
          {
            model: Aluno,
            as: 'aluno',
            attributes: ['id', 'nome']
          },
          {
            model: Professor,
            as: 'professor',
            attributes: ['id', 'nome']
          }
        ]
      });

      const disponibilidade_ids = req.body.disponibilidade_id.map(Number);
      Disponibilidade.update(
        {
          disponivel: false
        },
        {
          where: {
            id: { [Op.in]: disponibilidade_ids }
          }
        }
      );

      return res.json(aulaReturn);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async update(req, res) {
    // atualizacao de dados do plano
    const schema = Yup.object().shape({
      descricao: Yup.string()
    });

    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: 'Verifique os campos obrigatórios!' });
    }
    try {
      const aula = await Aula.findByPk(req.params.id); // find by primary key

      const { id } = await aula.update(req.body);
      const aulaReturn = await Aula.findAll({
        where: { id },
        attributes: ['id', 'data_inicio_previsto', 'data_fim_previsto'],
        include: [
          {
            model: Aluno,
            as: 'aluno',
            attributes: ['id', 'nome']
          },
          {
            model: Professor,
            as: 'professor',
            attributes: ['id', 'nome']
          }
        ]
      });
      return res.json(aulaReturn);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async delete(req, res) {
    try {
      const aula = await Aula.findByPk(req.params.id);
      if (!aula)
        return res.status(200).json({
          error: 'Aula não existente ou já cancelada.'
        });
      if (req.tipo_usuario === 'aluno') {
        if (req.usuarioId !== aula.aluno_id)
          return res.status(200).json({
            error: 'Você não pode deletar uma aula que não seja sua.'
          });
      } else if (req.tipo_usuario === 'professor') {
        if (req.usuarioId !== aula.professor_id)
          return res.status(200).json({
            error: 'Você não pode deletar uma aula que não seja sua.'
          });
      }

      if (aula.aconteceu)
        return res.status(200).json({
          error: 'Você não pode deletar uma aula que já ocorreu.'
        });

      const data_inicio = parseISO(req.body.data_inicio_previsto); // passando a data para formato js
      const data_atual = new Date();
      /**
       * verificando se data informada
       * esta no passado
       */
      if (isBefore(data_inicio, data_atual)) {
        return res
          .status(200)
          .json({ error: `Você não pode deletar uma aula no passado` });
      }

      await aula.destroy();

      return res.json({ success: 'Aula cancelada com sucesso!' });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new AulaController();
