import * as Yup from 'yup';
import Sequelize, { Op } from 'sequelize';
import { parseISO, isBefore } from 'date-fns';
import Disponibilidade from '../models/Disponibilidade';

class DisponibilidadeController {
  async index(req, res) {
    try {
      const disponibilidade = await Disponibilidade.findAll({
        attributes: ['id', 'data', 'disponivel'],
        where: {
          professor_id: req.params.id,
          data: {
            [Op.gte]: Sequelize.fn('NOW')
          }
        }
      });
      console.log(typeof disponibilidade);
      if (disponibilidade.length === 0) {
        return res
          .status(200)
          .json({ error: 'Este professor não possui horários disponíveis' });
      }
      return res.status(200).json(disponibilidade);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async store(req, res) {
    // criacao de plano
    const schema = Yup.object().shape({
      datas: Yup.string().required(() => {
        return res
          .status(200)
          .json({ error: 'Informe as datas que deseja alocar!' });
      })
    });
    /**
     * De a cordo com as regras a cima
     * validamos os dados com o yupi
     */
    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: 'Verifique os campos obrigatórios!' });
    }
    try {
      if (req.tipo_usuario !== 'professor') {
        return res.status(200).json({
          error: 'Somente professores podem alocar horários na agenda!'
        });
      }

      const hora_inicio = parseISO(req.body.datas[0]); // passando a data para formato js
      const data_atual = new Date();
      /**
       * verificando se data informada
       * estah no passado
       */
      if (isBefore(hora_inicio, data_atual)) {
        return res.status(200).json({ error: `Selecione um data no futuro!` });
      }

      // eslint-disable-next-line consistent-return
      const promises = req.body.datas.map(async data => {
        await Disponibilidade.create({
          professor_id: req.usuarioId,
          data,
          disponivel: true
        });
      });

      await Promise.all(promises);

      return res.status(200).json({ success: true });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new DisponibilidadeController();
