import Aluno from '../models/Aluno';
import File from '../models/File';
import Disciplina from '../models/Disciplina';
import Materia from '../models/Materia';

class FavoritoController {
  async index(req, res) {
    try {
      const aluno = await Aluno.findByPk(req.usuarioId); // find by primary key
      const favoritos = await aluno.getProfessores_favoritos({
        attributes: {
          exclude: ['senha_hash', 'createdAt', 'updatedAt']
        },
        include: [
          {
            model: File,
            as: 'avatar',
            attributes: ['id', 'path']
          },
          {
            model: Materia,
            as: 'materias',
            through: { attributes: [] }, // nao retornar dados da tabela pivot
            attributes: {
              exclude: ['createdAt', 'updatedAt', 'disciplina_id']
            },
            include: [
              {
                model: Disciplina,
                as: 'disciplina',
                attributes: {
                  exclude: [
                    'createdAt',
                    'updatedAt',
                    'imagemdark_id',
                    'imagemwhite_id'
                  ]
                },
                include: [
                  {
                    model: File,
                    as: 'thumb_white',
                    attributes: {
                      exclude: ['createdAt', 'updatedAt']
                    }
                  },
                  {
                    model: File,
                    as: 'thumb_dark',
                    attributes: {
                      exclude: ['createdAt', 'updatedAt']
                    }
                  }
                ]
              }
            ]
          }
        ]
      });
      // console.log(favoritos);
      // aluno.addFavoritos(professor_id);
      return res.status(200).json({ favoritos });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async store(req, res) {
    if (!req.params.id) {
      return res
        .status(200)
        .json({ error: 'Informe o professor que deseja favoritar!' });
    }
    try {
      const professor_id = req.params.id;
      const aluno = await Aluno.findByPk(req.usuarioId); // find by primary key
      aluno.addProfessores_favoritos(professor_id);
      return res.status(200).json({ success: true });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async delete(req, res) {
    if (!req.params.id) {
      return res
        .status(200)
        .json({ error: 'Informe o professor que deseja favoritar!' });
    }
    try {
      const professor_id = req.params.id;
      const aluno = await Aluno.findByPk(req.usuarioId); // find by primary key
      aluno.removeProfessores_favoritos(professor_id);
      return res.status(200).json({ success: true });
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }
}

export default new FavoritoController();
