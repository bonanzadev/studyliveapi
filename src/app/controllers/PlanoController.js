import * as Yup from 'yup';
import Plano from '../models/Plano';

class PlanoController {
  async index(req, res) {
    try {
      const plano = await Plano.findAll({
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'deletedAt']
        }
      });
      return res.status(200).json(plano);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async show(req, res) {
    try {
      const plano = await Plano.findAll({
        attributes: {
          exclude: ['createdAt', 'updatedAt', 'deletedAt']
        },
        where: {
          id: req.params.id
        }
      });
      return res.status(200).json(plano);
    } catch (error) {
      return res
        .status(500)
        .json({ error: 'Ops... estamos passando por uma instabilidade!' });
    }
  }

  async store(req, res) {
    // criacao de plano
    const schema = Yup.object().shape({
      descricao: Yup.string().required(() => {
        return res.status(200).json({ error: `Preencha o campo "descrição"!` });
      }),
      quantidade_minuto: Yup.string().required(() => {
        return res
          .status(200)
          .json({ error: `Preencha o campo "quantidade_minuto"!` });
      }),
      valor: Yup.string().required(() => {
        return res.status(200).json({ error: `Preencha o campo "valor"!` });
      })
    });

    /**
     * De a cordo com as regras a cima
     * validamos os dados com o yupi
     */
    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: `Verifique os campos obrigatórios!` });
    }
    try {
      const { id, descricao, quantidade_minuto, valor } = await Plano.create(
        req.body
      );

      return res.json({
        id,
        descricao,
        quantidade_minuto,
        valor
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: `Ops... estamos passando por uma instabilidade!` });
    }
  }

  async update(req, res) {
    // atualizacao de dados do plano
    const schema = Yup.object().shape({
      descricao: Yup.string()
    });

    if (!(await schema.isValid(req.body))) {
      return res
        .status(200)
        .json({ error: `Verifique os campos obrigatórios!` });
    }
    try {
      const plano = await Plano.findByPk(req.params.id); // find by primary key

      const { id, descricao } = await plano.update(req.body);

      return res.json({
        id,
        descricao
      });
    } catch (error) {
      return res
        .status(500)
        .json({ error: `Ops... estamos passando por uma instabilidade!` });
    }
  }
}

export default new PlanoController();
