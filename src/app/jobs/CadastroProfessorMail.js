import Mail from '../../lib/Mail';

class CadastroProfessorMail {
  get key() {
    return 'CadastroProfessorMail';
  }

  async handle({ data }) {
    const { nome, email } = data;

    console.log('A fila Executou');
    await Mail.sendMail({
      to: `${nome} <${email}>`,
      subject: 'Bem vindo(a) ao StudyLive! Sua plataforma de ensino',
      template: 'cadastroProfessor',
      context: {
        nome,
        link: 'https://google.com.br'
      }
    });
  }
}
export default new CadastroProfessorMail();
