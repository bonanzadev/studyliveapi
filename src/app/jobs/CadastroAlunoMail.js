import Mail from '../../lib/Mail';

class CadastroAlunoMail {
  get key() {
    return 'CadastroAlunoMail';
  }

  async handle({ data }) {
    const { nome, email } = data;

    console.log('A fila Executou');
    await Mail.sendMail({
      to: `${nome} <${email}>`,
      subject: 'Bem vindo(a) ao StudyLive! Sua plataforma de ensino',
      template: 'cadastroAluno',
      context: {
        nome,
        link: 'https://google.com.br'
      }
    });
  }
}
export default new CadastroAlunoMail();
