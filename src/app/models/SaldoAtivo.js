import Sequelize, { Model } from 'sequelize';

class SaldoAtivo extends Model {
  static init(sequelize) {
    super.init(
      {
        minutos: Sequelize.INTEGER,
        data_inicio: Sequelize.DATE,
        data_fim: Sequelize.DATE
      },
      { tableName: 'saldoativo', sequelize }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Aluno, { foreignKey: 'aluno_id', as: 'aluno' });
  }
}

export default SaldoAtivo;
