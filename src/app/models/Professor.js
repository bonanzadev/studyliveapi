import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Professor extends Model {
  static init(sequelize) {
    super.init(
      {
        nome: Sequelize.STRING,
        apelido: Sequelize.STRING,
        email: Sequelize.STRING,
        senha: Sequelize.VIRTUAL, // este campo nunca existira na base de dados, somente aqui
        senha_hash: Sequelize.STRING,
        celular: Sequelize.STRING,
        data_nascimento: Sequelize.DATE,
        saldo: Sequelize.INTEGER,
        ativo: Sequelize.BOOLEAN,
        confirmou_email: Sequelize.BOOLEAN,
        avaliacao_media: Sequelize.FLOAT,
        token_fcm: Sequelize.STRING,
        codigo_ativacao: Sequelize.STRING,
        configurou_dados: Sequelize.BOOLEAN,
        cep: Sequelize.STRING,
        logradouro: Sequelize.STRING,
        numero: Sequelize.STRING,
        complemento: Sequelize.STRING,
        bairro: Sequelize.STRING,
        video: Sequelize.TEXT,
        texto_apresentacao: Sequelize.STRING,
        texto_curriculo: Sequelize.STRING
      },
      {
        tableName: 'professor',
        sequelize
      }
    );
    this.addHook('beforeSave', async professor => {
      /**
       * hooks sao trechos de codigos que sao
       * executados de forma automatica baseado em acoes que acontecem em nosso model
       * ex: beforesave: antes de qualquer professor ser salvo ou editado no banco de dados ele
       * executa esta funcao de callback
       */
      if (professor.senha) {
        /**
         * somente executar este codigo quando
         * o usuario estiver informando uma senha nova
         */
        professor.senha_hash = await bcrypt.hash(professor.senha, 8); // o segundo parametro significa o numero da forca da criptografia. ps: quanto maior o numero, mais custoso em nivel de hardaware sera
      }
    });

    return this;
  }

  static associate(models) {
    this.belongsTo(models.File, { foreignKey: 'avatar_id', as: 'avatar' });
    this.belongsToMany(models.Materia, {
      through: 'professormateria',
      as: 'materias',
      foreignKey: 'professor_id'
    });
    this.belongsToMany(models.Aluno, {
      through: 'favorito',
      as: 'favoritos',
      foreignKey: 'professor_id'
    });
  }

  verificaSenha(senha) {
    /**
     * retorna true se a senha informada pelo usuario e igual a registrada no banco
     */
    return bcrypt.compare(senha, this.senha_hash);
  }
}

export default Professor;
