import Sequelize, { Model } from 'sequelize';

class Disciplina extends Model {
  static init(sequelize) {
    super.init(
      {
        nome: Sequelize.STRING,
        descricao: Sequelize.STRING,
        ativo: Sequelize.BOOLEAN
      },
      { tableName: 'disciplina', sequelize }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.File, {
      foreignKey: 'imagemdark_id',
      as: 'thumb_dark'
    });
    this.belongsTo(models.File, {
      foreignKey: 'imagemwhite_id',
      as: 'thumb_white'
    });
  }
}

export default Disciplina;
