import Sequelize, { Model } from 'sequelize';

class Materia extends Model {
  static init(sequelize) {
    super.init(
      {
        nome: Sequelize.STRING,
        descricao: Sequelize.STRING
      },
      { tableName: 'materia', sequelize }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Disciplina, {
      foreignKey: 'disciplina_id',
      as: 'disciplina'
    });
    this.belongsToMany(models.Professor, {
      through: 'professormateria',
      as: 'professores',
      foreignKey: 'materia_id'
    });
  }
}

export default Materia;
