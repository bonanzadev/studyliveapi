import Sequelize, { Model } from 'sequelize';
import bcrypt from 'bcryptjs';

class Aluno extends Model {
  static init(sequelize) {
    super.init(
      {
        nome: Sequelize.STRING,
        apelido: Sequelize.STRING,
        email: Sequelize.STRING,
        senha: Sequelize.VIRTUAL, // este campo nunca existira na base de dados, somente aqui
        senha_hash: Sequelize.STRING,
        celular: Sequelize.STRING,
        data_nascimento: Sequelize.DATE,
        nome_responsavel: Sequelize.STRING,
        celular_responsavel: Sequelize.STRING,
        ativo: Sequelize.BOOLEAN,
        confirmou_email: Sequelize.BOOLEAN,
        avaliacao_media: Sequelize.FLOAT,
        token_fcm: Sequelize.STRING,
        codigo_ativacao: Sequelize.STRING,
        codigo_indicacao: Sequelize.STRING
      },
      {
        tableName: 'aluno',
        sequelize
      }
    );
    this.addHook('beforeSave', async aluno => {
      /**
       * hooks sao trechos de codigos que sao
       * executados de forma automatica baseado em acoes que acontecem em nosso model
       * ex: beforesave: antes de qualquer aluno ser salvo ou editado no banco de dados ele
       * executa esta funcao de callback
       */
      if (aluno.senha) {
        /**
         * somente executar este codigo quando
         * o usuario estiver informando uma senha nova
         */
        aluno.senha_hash = await bcrypt.hash(aluno.senha, 8); // o segundo parametro significa o numero da forca da criptografia. ps: quanto maior o numero, mais custoso em nivel de hardaware sera
      }
    });

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Plano, { foreignKey: 'plano_id', as: 'plano' });
    this.belongsTo(models.File, { foreignKey: 'avatar_id', as: 'avatar' });
    this.belongsToMany(models.Professor, {
      through: 'favorito',
      as: 'professores_favoritos',
      foreignKey: 'aluno_id'
    });
    this.belongsTo(models.Aluno, {
      foreignKey: 'alunoindicou_id',
      as: 'aluno_indicou'
    });
  }

  verificaSenha(senha) {
    /**
     * retorna true se a senha informada pelo usuario e igual a registrada no banco
     */
    return bcrypt.compare(senha, this.senha_hash);
  }
}

export default Aluno;
