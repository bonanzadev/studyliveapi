import Sequelize, { Model } from 'sequelize';

class Aula extends Model {
  static init(sequelize) {
    super.init(
      {
        data_inicio_previsto: Sequelize.DATE,
        data_fim_previsto: Sequelize.DATE,
        data_inicio_real: Sequelize.DATE,
        data_fim_real: Sequelize.DATE,
        agendada: Sequelize.BOOLEAN,
        aconteceu: Sequelize.BOOLEAN,
        channel: Sequelize.STRING,
        avaliacao_professor: Sequelize.INTEGER,
        avaliacao_professor_mensagem: Sequelize.STRING,
        avaliacao_aluno: Sequelize.INTEGER,
        avaliacao_aluno_mensagem: Sequelize.STRING
      },
      { tableName: 'aula', paranoid: true, sequelize }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Aluno, {
      foreignKey: 'aluno_id',
      as: 'aluno'
    });
    this.belongsTo(models.Professor, {
      foreignKey: 'professor_id',
      as: 'professor'
    });
  }
}

export default Aula;
