import Sequelize, { Model } from 'sequelize';

class Plano extends Model {
  static init(sequelize) {
    super.init(
      {
        descricao: Sequelize.STRING,
        ativo: Sequelize.BOOLEAN,
        recorrente: Sequelize.BOOLEAN,
        quantidade_minuto: Sequelize.INTEGER,
        valor: Sequelize.FLOAT,
        experimental: Sequelize.BOOLEAN
      },
      { tableName: 'plano',  paranoid: true, sequelize }
    );

    return this;
  }
}

export default Plano;
