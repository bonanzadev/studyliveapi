import Sequelize, { Model } from 'sequelize';

class FaleConosco extends Model {
  static init(sequelize) {
    super.init(
      {
        motivo: Sequelize.STRING,
        email_contato: Sequelize.STRING,
        mensagem: Sequelize.STRING
      },
      { tableName: 'faleconosco', sequelize }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Aluno, {
      foreignKey: 'aluno_id',
      as: 'aluno'
    });
    this.belongsTo(models.Professor, {
      foreignKey: 'professor_id',
      as: 'professor'
    });
  }
}

export default FaleConosco;
