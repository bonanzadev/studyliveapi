import Sequelize, { Model } from 'sequelize';

class Disponibilidade extends Model {
  static init(sequelize) {
    super.init(
      {
        data: Sequelize.DATE,
        disponivel: Sequelize.BOOLEAN
      },
      { tableName: 'disponibilidade', sequelize }
    );

    return this;
  }

  static associate(models) {
    this.belongsTo(models.Professor, {
      foreignKey: 'professor_id',
      as: 'professor'
    });
  }
}

export default Disponibilidade;
