import Sequelize, { Model } from 'sequelize';

class File extends Model {
  static init(sequelize) {
    super.init(
      {
        nome: Sequelize.STRING,
        path: Sequelize.STRING
      },
      {
        tableName: 'file',
        sequelize
      }
    );

    return this;
  }
}

export default File;
