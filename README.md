#Iniciar o projeto

1. docker-compose up
2. npm install
3. npx sequelize db:migrate
4. npm run dev

#Comandos

* docker-compose up : executa o arquivo docker-compose.yml
que contem as regras para criação do ambiente.

* npm install : executa o arquivo package.json que contem as depencias do projeto.

* npx sequelize migration:create --name=create|update|delete-tabela : cria um arquvio de migrations.

* npx sequelize db:migration : executa as migrations pendentes.

* npx sequelize db:migration:undo : desfaz as migrations executadas no comando a cima.

* npm run dev : executa um script definido dentro do package.json
