import request from 'supertest';
import app from '../../src/app';
import truncate from '../util/truncate';

describe('Aluno', () => {
  beforeEach(async () => {
    await truncate();
  });
  it('Deve ser possível o aluno se cadastrar', async () => {
    const response = await request(app)
      .post('/aluno')
      .send({
        nome: 'Gabriel Nascimento',
        email: 'gabriel123@gmail.com',
        data_nascimento: '1997-11-30',
        celular: '27997233386',
        senha: '123456'
      });
    expect(response.body).toHaveProperty('id');
  });
  it('O aluno nao pode se cadastrar na aplicacao com email duplicado', async () => {
    await request(app)
      .post('/aluno')
      .send({
        nome: 'Gabriel Nascimento',
        email: 'gabriel321@gmail.com',
        data_nascimento: '1997-11-30',
        celular: '27997233386',
        senha: '123456'
      });
    const response = await request(app)
      .post('/aluno')
      .send({
        nome: 'Gabriel Nascimento',
        email: 'gabriel321@gmail.com',
        data_nascimento: '1997-11-30',
        celular: '27997233386',
        senha: '123456'
      });
    expect(response.body).toHaveProperty('error');
  });
}, 10000);
